#include <CppUTest/CommandLineTestRunner.h>

#include "FlashDriverTest.h"

int main(int argc, char* argv[])
{
	return CommandLineTestRunner::RunAllTests(argc, argv);
}
