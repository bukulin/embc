#include "FakeMicroTime.h"

static uint32_t time;
static uint32_t increment;

void FakeMicroTime_Init(uint32_t aStart,
			uint32_t aIncrement)
{
	time = aStart;
	increment = aIncrement;
}

uint32_t MicroTime_Get(void)
{
	uint32_t t = time;
	time += increment;
	return t;
}
