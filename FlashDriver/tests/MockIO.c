#include "MockIO.h"

#include <CppUTestExt/MockSupport_c.h>

void IO_Write(ioAddress addr, ioData data)
{
	mock_c()->actualCall(__FUNCTION__)->
		withIntParameters("addr", addr)->
		withIntParameters("data", data);
}

ioData IO_Read(ioAddress addr)
{
	mock_c()->actualCall(__FUNCTION__)->
		withIntParameters("addr", addr);
	return mock_c()->returnValue().value.intValue;
}
