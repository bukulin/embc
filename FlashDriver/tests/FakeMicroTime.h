#ifndef FakeMicroTime_h
#define FakeMicroTime_h 1

#include "RTC.h"

void FakeMicroTime_Init(uint32_t start, uint32_t increment);

#endif
