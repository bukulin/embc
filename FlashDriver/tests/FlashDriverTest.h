#ifndef FlashTest_h
#define FlashTest_h 1

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

extern "C"
{
#include "FlashDriver.h"
#include "m28w160ect.h"
#include "FakeMicroTime.h"
}

TEST_GROUP(Flash)
{
	ioAddress address;
	ioData data;
	int result;

	TEST_SETUP()
	{
		address = 0x1000;
		data = 0xbeef;
		result = -1;
		Flash_Create();
	}

	TEST_TEARDOWN()
	{
		mock().checkExpectations();
		mock().clear();
		Flash_Destroy();
	}

	void mockExpectWrite(ioAddress address, ioData data)
	{
		mock().expectOneCall("IO_Write").
			withParameter("addr", address).
			withParameter("data", data);
	}
	void mockExpectRead(ioAddress address, ioData returnValue)
	{
		mock().expectOneCall("IO_Read").
			withParameter("addr", address).
			andReturnValue((int)returnValue);
	}
};

TEST(Flash, WriteSuccessful_ReadyImmediately)
{
	mockExpectWrite(CommandRegister, ProgramCommand);
	mockExpectWrite(address, data);
	mockExpectRead(StatusRegister, ReadyBit);
	mockExpectRead(address, data);

	result = Flash_Write(address, data);

	LONGS_EQUAL(FLASH_SUCCES, result);
}

TEST(Flash, ProgramSucceedsNotImmediatelyReady)
{
	mockExpectWrite(CommandRegister, ProgramCommand);
	mockExpectWrite(address, data);
	mockExpectRead(StatusRegister, 0);
	mockExpectRead(StatusRegister, 0);
	mockExpectRead(StatusRegister, 0);
	mockExpectRead(StatusRegister, ReadyBit);
	mockExpectRead(address, data);

	result = Flash_Write(address, data);

	LONGS_EQUAL(FLASH_SUCCES, result);
}

TEST(Flash, WriteFails_VppError)
{
	mockExpectWrite(CommandRegister, ProgramCommand);
	mockExpectWrite(address, data);
	mockExpectRead(StatusRegister, ReadyBit | VppErrorBit);
	mockExpectWrite(CommandRegister, Reset);

	result = Flash_Write(address, data);

	LONGS_EQUAL(FLASH_VPP_ERROR, result);
}

TEST(Flash, WriteFails_ProgramError)
{
	mockExpectWrite(CommandRegister, ProgramCommand);
	mockExpectWrite(address, data);
	mockExpectRead(StatusRegister, ReadyBit | ProgramErrorBit);
	mockExpectWrite(CommandRegister, Reset);

	result = Flash_Write(address, data);

	LONGS_EQUAL(FLASH_PROGRAM_ERROR, result);
}

TEST(Flash, WriteFails_ProtectedBlockError)
{
	mockExpectWrite(CommandRegister, ProgramCommand);
	mockExpectWrite(address, data);
	mockExpectRead(StatusRegister, ReadyBit | ProtectedBlockErrorBit);
	mockExpectWrite(CommandRegister, Reset);

	result = Flash_Write(address, data);

	LONGS_EQUAL(FLASH_PROTECTED_BLOCK_ERROR, result);
}

TEST(Flash, WriteFails_UnknownError)
{
	mockExpectWrite(CommandRegister, ProgramCommand);
	mockExpectWrite(address, data);
	mockExpectRead(StatusRegister, ReadyBit | (1 << 2));
	mockExpectWrite(CommandRegister, Reset);

	result = Flash_Write(address, data);

	LONGS_EQUAL(FLASH_UNKNOWN_ERROR, result);
}

TEST(Flash, WriteFails_FlashReadBackError)
{
	mockExpectWrite(CommandRegister, ProgramCommand);
	mockExpectWrite(address, data);
	mockExpectRead(StatusRegister, ReadyBit);
	mockExpectRead(address, data - 1);

	result = Flash_Write(address, data);

	LONGS_EQUAL(FLASH_READ_BACK_ERROR, result);
}

TEST(Flash, WriteSuccessful_IgnoreOtherBitsUntilReady)
{
	mockExpectWrite(CommandRegister, ProgramCommand);
	mockExpectWrite(address, data);
	mockExpectRead(StatusRegister, ~ReadyBit);
	mockExpectRead(StatusRegister, ReadyBit);
	mockExpectRead(address, data);

	result = Flash_Write(address, data);

	LONGS_EQUAL(FLASH_SUCCES, result);
}

TEST(Flash, WriteFails_Timeout)
{
	FakeMicroTime_Init(0, 500);
	Flash_Create();
	mockExpectWrite(CommandRegister, ProgramCommand);
	mockExpectWrite(address, data);
	for(int i = 0; i < 10; ++i)
		mockExpectRead(StatusRegister, ~ReadyBit);

	result = Flash_Write(address, data);

	LONGS_EQUAL(FLASH_TIMEOUT_ERROR, result);
}

TEST(Flash, WriteFails_TimeoutAtEndOfTime)
{
	FakeMicroTime_Init(0xffffffff, 500);
	Flash_Create();
	mockExpectWrite(CommandRegister, ProgramCommand);
	mockExpectWrite(address, data);
	for(int i = 0; i < 10; ++i)
		mockExpectRead(StatusRegister, ~ReadyBit);

	result = Flash_Write(address, data);

	LONGS_EQUAL(FLASH_TIMEOUT_ERROR, result);
}

#endif
