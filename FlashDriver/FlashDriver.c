#include "FlashDriver.h"
#include "RTC.h"
#include "m28w160ect.h"

#define FLASH_WRITE_TIMEOUT_IN_MICROSECONDS 5000

void Flash_Create()
{
}
void Flash_Destroy()
{
}

static int writeError(ioData status)
{
	IO_Write(CommandRegister, Reset);

	if(status & VppErrorBit)
		return FLASH_VPP_ERROR;
	if(status & ProgramErrorBit)
		return FLASH_PROGRAM_ERROR;
	if(status & ProtectedBlockErrorBit)
		return FLASH_PROTECTED_BLOCK_ERROR;

	return FLASH_UNKNOWN_ERROR;
}

int Flash_Write(ioAddress addr, ioData data)
{
	ioData status = 0;
	uint32_t timestamp = MicroTime_Get();

	IO_Write(CommandRegister, ProgramCommand);
	IO_Write(addr, data);

	while( (status & ReadyBit) == 0 )
	{
		if(MicroTime_Get() - timestamp > FLASH_WRITE_TIMEOUT_IN_MICROSECONDS)
			return FLASH_TIMEOUT_ERROR;
		status = IO_Read(StatusRegister);
	}

	if(status != ReadyBit)
		return writeError(status);

	if(data != IO_Read(addr))
		return FLASH_READ_BACK_ERROR;

	return FLASH_SUCCES;
}
