#ifndef FlashDriver_h
#define FlashDriver_h 1

#include "IO.h"

enum
{
	FLASH_SUCCES = 0,
	FLASH_VPP_ERROR =  -1,
	FLASH_PROGRAM_ERROR =  -2,
	FLASH_PROTECTED_BLOCK_ERROR = -3,
	FLASH_UNKNOWN_ERROR = -4,
	FLASH_READ_BACK_ERROR = -5,
	FLASH_TIMEOUT_ERROR = -6
};

void Flash_Create();
void Flash_Destroy();

int Flash_Write(ioAddress addr, ioData data);

#endif
