#ifndef m28w160ect_h
#define m28w160ect_h 1

typedef enum
{
	CommandRegister = 0x0,
	StatusRegister = 0x0
} Flas_Registers;

typedef enum
{
	ProgramCommand = 0x40,
	Reset = 0xff
} Flash_Command;

#define ReadyBit (1 << 7)
#define ProgramErrorBit (1 << 4)
#define VppErrorBit (1 << 3)
#define ProtectedBlockErrorBit (1 << 1)

#endif
