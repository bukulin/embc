#ifndef IO_h
#define IO_h 1

typedef int ioAddress;
typedef int ioData;

void IO_Write(ioAddress addr, ioData data);
ioData IO_Read(ioAddress addr);

#endif
