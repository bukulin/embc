#ifndef LightScheduler_h
#define LightScheduler_h 1

#include "TimeService.h"
#include "RandomMinute.h"

enum LS_ErrorCodes
{
	LS_OK = 0,
	LS_TOO_MANY_EVENTS = -1,
	LS_NO_SUCH_SCHEDULED_EVENT = -2,
	LS_ID_OUT_OF_BOUNDS = -3
};
typedef enum LS_ErrorCodes LS_ErrorCodes;

void LightScheduler_Create(RandomMinute randomMinute);
void LightScheduler_Destroy(void);

LS_ErrorCodes LightScheduler_ScheduleTurnOn(int id, Day day, int minuteOfDay);
LS_ErrorCodes LightScheduler_ScheduleTurnOff(int id, Day day, int minuteOfDay);
LS_ErrorCodes LightScheduler_ScheduleRemove(int id, Day day, int minuteOfDay);

LS_ErrorCodes LightScheduler_Randomize(int id, Day day, int minuteOfDay);

void LightScheduler_Wakeup(void);

#endif
