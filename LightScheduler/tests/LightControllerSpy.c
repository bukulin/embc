#include "LightControllerSpy.h"

static int last_id;
static int last_state;

static int light_states[NUMBER_OF_LIGHTS];

void LightController_Create()
{
	int i;

	last_id = LIGHT_ID_UNKNOWN;
	last_state = LIGHT_STATE_UNKNOWN;
	for(i = 0; i < NUMBER_OF_LIGHTS; ++i)
		light_states[i] = LIGHT_STATE_UNKNOWN;
}
void LightController_Destroy()
{
}

static void operateLight(int id, int level)
{
	last_id = id;
	last_state = level;
	light_states[id] = level;
}

void LightController_On(int id)
{
	operateLight(id, LIGHT_ON);
}

void LightController_Off(int id)
{
	operateLight(id, LIGHT_OFF);
}

int LightControllerSpy_GetLightState(int id)
{
	if(id < 0 || id >= NUMBER_OF_LIGHTS)
		return LIGHT_STATE_UNKNOWN;
	return light_states[id];
}

int LightControllerSpy_GetLastId()
{
	return last_id;
}

int LightControllerSpy_GetLastState()
{
	return last_state;
}
