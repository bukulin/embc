#ifndef FakeTimeService_h
#define FakeTimeService_h 1

#include "TimeService.h"

void FakeTimeService_SetDay(int dayOfWeek);
void FakeTimeService_SetMinute(int minuteOfDay);

WakeupCallback FakeTimeService_GetAlarmCallback(void);
int FakeTimeService_GetAlarmPeriod(void);

#endif
