#include "FakeRandomMinute.h"
#include "RandomMinutePrivate.h"

#include <stdlib.h>

typedef struct FakeRandomMinuteStruct* FakeRandomMinute;
typedef struct FakeRandomMinuteStruct
{
	RandomMinuteStruct base;
	int value;
	int increment;
} FakeRandomMinuteStruct;

static int get(RandomMinute base)
{
	FakeRandomMinute self = (FakeRandomMinute)base;
	int tmpValue = self->value;
	self->value += self->increment;
	return tmpValue;
}

static void destroy(RandomMinute self)
{
	free(self);
}

static RandomMinuteInterfaceStruct interface =
{
	.Destroy = destroy,
	.Get = get
};

RandomMinute FakeRandomMinute_Create()
{
	FakeRandomMinute self = calloc(1, sizeof(FakeRandomMinuteStruct));
	self->base.vtable = &interface;
	return (RandomMinute)self;
}

void FakeRandomMinute_SetFirstAndIncrement(RandomMinute base,
					   int aFirst, int aIncrement)
{
	FakeRandomMinute self = (FakeRandomMinute)base;
	self->value = aFirst;
	self->increment = aIncrement;
	return;
}
