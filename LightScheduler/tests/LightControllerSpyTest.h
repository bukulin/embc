// -*- mode: c++ -*-
#ifndef LightControllerSpyTest_h
#define LightControllerSpyTest_h 1

#include <CppUTest/TestHarness.h>

extern "C"
{
#include "LightControllerSpy.h"
}

#define NUMBER_OF_LIGHTS 32

TEST_GROUP(LightControllerSpy)
{
	TEST_SETUP()
	{
		LightController_Create();
	}

	TEST_TEARDOWN()
	{
		LightController_Destroy();
	}
};

TEST(LightControllerSpy, Create)
{
	LONGS_EQUAL(LIGHT_ID_UNKNOWN, LightControllerSpy_GetLastId());
	LONGS_EQUAL(LIGHT_STATE_UNKNOWN, LightControllerSpy_GetLastState());
	for(int i = 0; i < NUMBER_OF_LIGHTS; ++i)
		LONGS_EQUAL(LIGHT_STATE_UNKNOWN, LightControllerSpy_GetLightState(i));
}

TEST(LightControllerSpy, RememberTheLastLightIdControlled)
{
	LightController_On(10);
	LONGS_EQUAL(10, LightControllerSpy_GetLastId());
	LONGS_EQUAL(LIGHT_ON, LightControllerSpy_GetLastState());
}

TEST(LightControllerSpy, RememberOffState)
{
	LightController_Off(11);
	LONGS_EQUAL(11, LightControllerSpy_GetLastId());
	LONGS_EQUAL(LIGHT_OFF, LightControllerSpy_GetLastState());
}

TEST(LightControllerSpy, RemberAllLightStates)
{
	LightController_On(0);
	LightController_Off(31);
	LONGS_EQUAL(LIGHT_ON, LightControllerSpy_GetLightState(0));
	LONGS_EQUAL(LIGHT_OFF, LightControllerSpy_GetLightState(31));
}

TEST(LightControllerSpy, OutOfBoundsShallBeUnknown)
{
	LONGS_EQUAL(LIGHT_STATE_UNKNOWN, LightControllerSpy_GetLightState(NUMBER_OF_LIGHTS + 1));
	LONGS_EQUAL(LIGHT_STATE_UNKNOWN, LightControllerSpy_GetLightState(-1));
}

#undef NUMBER_OF_LIGHTS

#endif
