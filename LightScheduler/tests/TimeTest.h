// -*- mode: c++ -*-
#ifndef TimeTest_h
#define TimeTest_h 1

#include <CppUTest/TestHarness.h>
#include "FakeTimeService.h"

TEST_GROUP(Time)
{
	Time time;

	TEST_SETUP()
	{
		TimeService_Create();
		time = Time_Create();
	}

	TEST_TEARDOWN()
	{
		Time_Destroy(time);
		TimeService_Destroy();
	}

	void givenThatItIs(Day day)
	{
		FakeTimeService_SetDay(day);
	}

	void CheckThatTimeMatches(Day day)
	{
		TimeService_GetTime(time);
		CHECK(Time_MatchesDayOfWeek(time, day));
	}

	void CheckThatTimeDoesNotMatch(Day day)
	{
		TimeService_GetTime(time);
		CHECK(!Time_MatchesDayOfWeek(time, day));
	}
};

TEST(Time, ZeroAfterCreate)
{
	Time time = Time_Create();
	CHECK(Time_MatchesDayOfWeek(time, DAY_UNKNOWN));
	CHECK(Time_MatchesMinuteOfDay(time, MINUTE_UNKNOWN));
	Time_Destroy(time);
}

TEST(Time, ExactMatch)
{
	givenThatItIs(MONDAY);
	CheckThatTimeMatches(MONDAY);
	givenThatItIs(TUESDAY);
	CheckThatTimeMatches(TUESDAY);
	givenThatItIs(WEDNESDAY);
	CheckThatTimeMatches(WEDNESDAY);
	givenThatItIs(THURSDAY);
	CheckThatTimeMatches(THURSDAY);
	givenThatItIs(FRIDAY);
	CheckThatTimeMatches(FRIDAY);
	givenThatItIs(SATURDAY);
	CheckThatTimeMatches(SATURDAY);
	givenThatItIs(SUNDAY);
	CheckThatTimeMatches(SUNDAY);
}

TEST(Time, WeenendDays)
{
	givenThatItIs(SATURDAY);
	CheckThatTimeMatches(WEEKEND);
	givenThatItIs(SUNDAY);
	CheckThatTimeMatches(WEEKEND);
}

TEST(Time, NotWeenendDays)
{
	givenThatItIs(MONDAY);
	CheckThatTimeDoesNotMatch(WEEKEND);
	givenThatItIs(TUESDAY);
	CheckThatTimeDoesNotMatch(WEEKEND);
	givenThatItIs(WEDNESDAY);
	CheckThatTimeDoesNotMatch(WEEKEND);
	givenThatItIs(THURSDAY);
	CheckThatTimeDoesNotMatch(WEEKEND);
	givenThatItIs(FRIDAY);
	CheckThatTimeDoesNotMatch(WEEKEND);
}

TEST(Time, Weekdays)
{
	givenThatItIs(MONDAY);
	CheckThatTimeMatches(WEEKDAY);
	givenThatItIs(TUESDAY);
	CheckThatTimeMatches(WEEKDAY);
	givenThatItIs(WEDNESDAY);
	CheckThatTimeMatches(WEEKDAY);
	givenThatItIs(THURSDAY);
	CheckThatTimeMatches(WEEKDAY);
	givenThatItIs(FRIDAY);
	CheckThatTimeMatches(WEEKDAY);
}

TEST(Time, NotWeekdays)
{
	givenThatItIs(SATURDAY);
	CheckThatTimeDoesNotMatch(WEEKDAY);
	givenThatItIs(SUNDAY);
	CheckThatTimeDoesNotMatch(WEEKDAY);
}

TEST(Time, Everyday)
{
	givenThatItIs(MONDAY);
	CheckThatTimeMatches(EVERYDAY);
	givenThatItIs(TUESDAY);
	CheckThatTimeMatches(EVERYDAY);
	givenThatItIs(WEDNESDAY);
	CheckThatTimeMatches(EVERYDAY);
	givenThatItIs(THURSDAY);
	CheckThatTimeMatches(EVERYDAY);
	givenThatItIs(FRIDAY);
	CheckThatTimeMatches(EVERYDAY);
	givenThatItIs(SATURDAY);
	CheckThatTimeMatches(EVERYDAY);
	givenThatItIs(SUNDAY);
	CheckThatTimeMatches(EVERYDAY);
}

#endif
