#ifndef FakeRandomMinuteTest_h
#define FakeRandomMinuteTest_h 1

#include <CppUTest/TestHarness.h>

extern "C"
{
#include "FakeRandomMinute.h"
}

TEST_GROUP(FakeRandomMinute)
{
	int minute;
	RandomMinute randomMinute;

	TEST_SETUP()
	{
		randomMinute = FakeRandomMinute_Create();
	}

	TEST_TEARDOWN()
	{
		RandomMinute_Destroy(randomMinute);

	}
};

TEST(FakeRandomMinute, FirstShallBeEqualWithSetOne)
{
	FakeRandomMinute_SetFirstAndIncrement(randomMinute,
					      -10, 5);
	minute = RandomMinute_Get(randomMinute);
	LONGS_EQUAL(-10, minute);
}

TEST(FakeRandomMinute, CheckIncrement)
{
	FakeRandomMinute_SetFirstAndIncrement(randomMinute,
					      -10, 5);
	RandomMinute_Get(randomMinute);
	minute = RandomMinute_Get(randomMinute);
	LONGS_EQUAL((-10 + 5), minute);
}

#endif
