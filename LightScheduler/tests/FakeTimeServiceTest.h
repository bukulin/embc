// -*- mode: c++ -*-
#ifndef FakeTimeServiceTest_h
#define FakeTimeServiceTest_h 1

#include <CppUTest/TestHarness.h>

extern "C"
{
#include "FakeTimeService.h"
#include "LightScheduler.h"
}

TEST_GROUP(FakeTimeService)
{
	TEST_SETUP()
	{
		TimeService_Create();
	}

	TEST_TEARDOWN()
	{
		TimeService_Destroy();
	}
};

TEST(FakeTimeService, Create)
{
	Time time = Time_Create();
	TimeService_GetTime(time);

	CHECK(Time_MatchesDayOfWeek(time, DAY_UNKNOWN));
	CHECK(Time_MatchesMinuteOfDay(time, MINUTE_UNKNOWN));

	POINTERS_EQUAL(NULL, FakeTimeService_GetAlarmCallback());
	LONGS_EQUAL(MINUTE_UNKNOWN, FakeTimeService_GetAlarmPeriod());

	Time_Destroy(time);
}

TEST(FakeTimeService, Set)
{
	Time time = Time_Create();
	FakeTimeService_SetMinute(42);
	FakeTimeService_SetDay(SATURDAY);

	TimeService_GetTime(time);

	CHECK(Time_MatchesDayOfWeek(time, SATURDAY));
	CHECK(Time_MatchesMinuteOfDay(time, 42));

	Time_Destroy(time);
}

extern "C"
{

static void dummyCallback(void)
{
}

}

TEST(FakeTimeService, SetPeriodicAlarm)
{
	TimeService_SetPeriodicAlarmInSeconds(10, dummyCallback);
	POINTERS_EQUAL((void*)dummyCallback,
		       FakeTimeService_GetAlarmCallback());
	LONGS_EQUAL(10,
		    FakeTimeService_GetAlarmPeriod());
}

TEST(FakeTimeService, CancelPeriodicAlarm)
{
	TimeService_SetPeriodicAlarmInSeconds(10, dummyCallback);
	TimeService_CancelPeriodicAlarmInSeconds(10, dummyCallback);
	POINTERS_EQUAL((void*)0,
		       FakeTimeService_GetAlarmCallback());
	LONGS_EQUAL(MINUTE_UNKNOWN,
		    FakeTimeService_GetAlarmPeriod());
}

TEST(FakeTimeService, CancelInequalTimePeriodicAlarm)
{
	TimeService_SetPeriodicAlarmInSeconds(10, dummyCallback);
	TimeService_CancelPeriodicAlarmInSeconds(20, dummyCallback);
	POINTERS_EQUAL((void*)dummyCallback,
		       FakeTimeService_GetAlarmCallback());
	LONGS_EQUAL(10,
		    FakeTimeService_GetAlarmPeriod());
}

TEST(FakeTimeService, CancelInequalCallbackPeriodicAlarm)
{
	TimeService_SetPeriodicAlarmInSeconds(10, dummyCallback);
	TimeService_CancelPeriodicAlarmInSeconds(10, (WakeupCallback)0);
	POINTERS_EQUAL((void*)dummyCallback,
		       FakeTimeService_GetAlarmCallback());
	LONGS_EQUAL(10,
		    FakeTimeService_GetAlarmPeriod());
}

#endif
