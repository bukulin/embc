#ifndef LightControllerSpy_h
#define LightControllerSpy_h 1

#include "LightController.h"

enum
{
	LIGHT_ID_UNKNOWN = -1, LIGHT_STATE_UNKNOWN = -1,
	LIGHT_OFF = 0, LIGHT_ON = 1
};

int LightControllerSpy_GetLightState(int id);
int LightControllerSpy_GetLastId();
int LightControllerSpy_GetLastState();

#endif
