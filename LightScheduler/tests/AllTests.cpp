#include <CppUTest/CommandLineTestRunner.h>

#include "FakeRandomMinuteTest.h"
#include "FakeTimeServiceTest.h"
#include "LightControllerSpyTest.h"
#include "LightSchedulerRandomizeTest.h"
#include "LightSchedulerTest.h"
#include "TimeTest.h"

int main(int argc, char* argv[])
{
	return CommandLineTestRunner::RunAllTests(argc, argv);
}
