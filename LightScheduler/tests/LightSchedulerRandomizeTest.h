// -*- mode: c++ -*-
#ifndef LightSchedulerRandomizeTest_h
#define LightSchedulerRandomizeTest_h 1

#include <CppUTest/TestHarness.h>

extern "C"
{
#include "FakeRandomMinute.h"
}

TEST_GROUP(LightSchedulerRandomize)
{
	RandomMinute randomMinute;
	TEST_SETUP()
	{
		LightController_Create();
		randomMinute = FakeRandomMinute_Create();
		LightScheduler_Create(randomMinute);
		FakeRandomMinute_SetFirstAndIncrement(randomMinute, -10, 5);
	}

	TEST_TEARDOWN()
	{
		LightScheduler_Destroy();
		RandomMinute_Destroy(randomMinute);
		LightController_Destroy();
	}

	void setTimeTo(Day day, int minuteOfDay)
	{
		FakeTimeService_SetDay(day);
		FakeTimeService_SetMinute(minuteOfDay);
	}

	void checkLightState(int id, int level)
	{
		if(id == LIGHT_ID_UNKNOWN)
		{
			LONGS_EQUAL(id, LightControllerSpy_GetLastId());
			LONGS_EQUAL(level, LightControllerSpy_GetLastState());
		}
		else
			LONGS_EQUAL(level, LightControllerSpy_GetLightState(id));
	}
};

TEST(LightSchedulerRandomize, TurnsOnEarly)
{
	LightScheduler_ScheduleTurnOn(4, EVERYDAY, 600);
	LightScheduler_Randomize(4, EVERYDAY, 600);

	setTimeTo(MONDAY, 600-10);

	LightScheduler_Wakeup();

	checkLightState(4, LIGHT_ON);
}

TEST(LightSchedulerRandomize, TurnsOnLater)
{
	FakeRandomMinute_SetFirstAndIncrement(randomMinute, 10, 5);
	LightScheduler_ScheduleTurnOn(4, EVERYDAY, 600);
	LightScheduler_Randomize(4, EVERYDAY, 600);

	setTimeTo(MONDAY, 600+10);

	LightScheduler_Wakeup();

	checkLightState(4, LIGHT_ON);
}


TEST(LightSchedulerRandomize, RandomizeScheduledEventIsOK)
{
	LightScheduler_ScheduleTurnOn(4, EVERYDAY, 600);
	LONGS_EQUAL(LS_OK,
		    LightScheduler_Randomize(4, EVERYDAY, 600));
}

TEST(LightSchedulerRandomize, DoNotRandomizeNonScheduledEvent)
{
	LightScheduler_ScheduleTurnOn(4, EVERYDAY, 600);

	LONGS_EQUAL(LS_NO_SUCH_SCHEDULED_EVENT,
		    LightScheduler_Randomize(5, EVERYDAY, 600));
}

#endif
