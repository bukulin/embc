#include "FakeTimeService.h"

static int dayOfWeek;
static int minuteOfDay;
static WakeupCallback callback;
static int alarmPeriod;

typedef struct TimeStruct
{
	int minuteOfDay;
	int dayOfWeek;
} TimeStruct;

void TimeService_Create()
{
	dayOfWeek = DAY_UNKNOWN;
	minuteOfDay = MINUTE_UNKNOWN;
	alarmPeriod = MINUTE_UNKNOWN;
	callback = 0;
}
void TimeService_Destroy()
{
}

void TimeService_GetTime(Time time)
{
	time->dayOfWeek = dayOfWeek;
	time->minuteOfDay = minuteOfDay;
}
void TimeService_SetPeriodicAlarmInSeconds(int aSeconds, WakeupCallback aCallback)
{
	alarmPeriod = aSeconds;
	callback = aCallback;
}
void TimeService_CancelPeriodicAlarmInSeconds(int aSeconds, WakeupCallback aCallback)
{
	if(aSeconds != alarmPeriod || aCallback != callback)
		return;
	alarmPeriod = MINUTE_UNKNOWN;
	callback = 0;
}

void FakeTimeService_SetDay(int aDayOfWeek)
{
	dayOfWeek = aDayOfWeek;
}

void FakeTimeService_SetMinute(int aMinuteOfDay)
{
	minuteOfDay = aMinuteOfDay;
}

WakeupCallback FakeTimeService_GetAlarmCallback(void)
{
	return callback;
}
int FakeTimeService_GetAlarmPeriod(void)
{
	return alarmPeriod;
}
