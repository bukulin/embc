#ifndef FakeRandomMinute_h
#define FakeRandomMinute_h 1

#include "RandomMinute.h"

RandomMinute FakeRandomMinute_Create();
void FakeRandomMinute_SetFirstAndIncrement(RandomMinute self,
					   int first, int increment);

#endif
