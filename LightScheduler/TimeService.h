#ifndef TimeService_h
#define TimeService_h 1

#include "Time.h"

void TimeService_Create();
void TimeService_Destroy();

void TimeService_GetTime(Time time);

typedef void (*WakeupCallback)(void);
void TimeService_SetPeriodicAlarmInSeconds(int seconds, WakeupCallback callback);
void TimeService_CancelPeriodicAlarmInSeconds(int seconds, WakeupCallback callback);

#endif
