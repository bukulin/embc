#ifndef Time_h
#define Time_h 1

#include "BOOL.h"

enum Day
{
	DAY_UNKNOWN = -1,

	EVERYDAY = 10,
	WEEKDAY,
	WEEKEND,

	MONDAY = 1,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY,
	SUNDAY
};
typedef enum Day Day;

enum
{
	MINUTE_UNKNOWN = -1
};

typedef struct TimeStruct* Time;

Time Time_Create(void);
void Time_Destroy(Time time);

BOOL Time_MatchesDayOfWeek(Time time, Day day);
BOOL Time_MatchesMinuteOfDay(Time time, int minuteOfDay);


#endif
