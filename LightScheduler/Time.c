#include "Time.h"

#include <stdlib.h>

typedef struct TimeStruct
{
	int minuteOfDay;
	int dayOfWeek;
} TimeStruct;

Time Time_Create(void)
{
	Time time = calloc(1, sizeof(TimeStruct));
	time->dayOfWeek = DAY_UNKNOWN;
	time->minuteOfDay = MINUTE_UNKNOWN;
	return time;
}
void Time_Destroy(Time time)
{
	free(time);
}

BOOL Time_MatchesDayOfWeek(Time time, Day day)
{
	Day today = time->dayOfWeek;
	if (day == EVERYDAY)
		return TRUE;
	if (day == today)
		return TRUE;
	if (day == WEEKEND && (today == SATURDAY || today == SUNDAY))
		return TRUE;
	if (day == WEEKDAY && (today >= MONDAY && today <= FRIDAY))
		return TRUE;
	return FALSE;
}

BOOL Time_MatchesMinuteOfDay(Time time, int minuteOfDay)
{
	return (time->minuteOfDay == minuteOfDay);
}
