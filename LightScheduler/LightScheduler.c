#include "LightScheduler.h"
#include "LightController.h"

enum
{
	UNUSED = -1
};

enum Event
{
	TURN_OFF = 0,
	TURN_ON = 1
};

enum Randomize
{
	RANDOM_OFF = 0,
	RANDOM_ON = 1
};

typedef struct
{
	int id;
	enum Event event;
	Day day;
	int minuteOfDay;
	int randomMinutes;
	enum Randomize randomize;
} ScheduledLightEvent;

#define MAX_EVENTS 128

static ScheduledLightEvent scheduledEvents[MAX_EVENTS];
static RandomMinute randomMinute;
static Time time;

void LightScheduler_Create(RandomMinute aRandomMinute)
{
	int i;

	for(i = 0; i < MAX_EVENTS; ++i)
		scheduledEvents[i].id = UNUSED;

	randomMinute = aRandomMinute;
	time = Time_Create();
	TimeService_SetPeriodicAlarmInSeconds(60,
					      LightScheduler_Wakeup);
}
void LightScheduler_Destroy()
{
	TimeService_CancelPeriodicAlarmInSeconds(60, LightScheduler_Wakeup);
	Time_Destroy(time);
}

static BOOL isIdAcceptable(int id)
{
	return (0 <= id && id < NUMBER_OF_LIGHTS);
}
static BOOL isInUse(ScheduledLightEvent* event)
{
	return event->id != UNUSED;
}
static ScheduledLightEvent* findUnusedEvent(void)
{
	int i;
	ScheduledLightEvent* event = scheduledEvents;

	for(i = 0; i < MAX_EVENTS; ++i, ++event)
	{
		if(!isInUse( event ))
			return event;
	}

	return 0;
}
static void setEventSchedule(ScheduledLightEvent* event,
			     int id, Day day, int minuteOfDay, enum Event control)
{
	event->id = id;
	event->event = control;
	event->day = day;
	event->minuteOfDay = minuteOfDay;
}
static LS_ErrorCodes scheduleEvent(int id, Day day, int minuteOfDay, enum Event control)
{
	ScheduledLightEvent* event = 0;

	if(!isIdAcceptable(id))
		return LS_ID_OUT_OF_BOUNDS;

	event = findUnusedEvent();
	if(event == 0)
		return LS_TOO_MANY_EVENTS;

	setEventSchedule(event,
			 id, day, minuteOfDay, control);
	return LS_OK;
}
LS_ErrorCodes LightScheduler_ScheduleTurnOn(int id, enum Day day, int minuteOfDay)
{
	return scheduleEvent(id, day, minuteOfDay, TURN_ON);
}
LS_ErrorCodes LightScheduler_ScheduleTurnOff(int id, enum Day day, int minuteOfDay)
{
	return scheduleEvent(id, day, minuteOfDay, TURN_OFF);
}

static BOOL eventMatches(ScheduledLightEvent* event,
			 int id, enum Day day, int minuteOfDay)
{
	if( event->id != id )
		return FALSE;
	if( event->day != day )
		return FALSE;
	if( event->minuteOfDay != minuteOfDay )
		return FALSE;
	return TRUE;
}
static int findScheduledEvent(int id, Day day, int minuteOfDay)
{
	int i;
	for(i = 0; i < MAX_EVENTS; ++i)
	{
		if( eventMatches(&scheduledEvents[i], id, day, minuteOfDay) )
			return i;
	}
	return UNUSED;
}
LS_ErrorCodes LightScheduler_ScheduleRemove(int id, Day day, int minuteOfDay)
{
	int index = findScheduledEvent(id, day, minuteOfDay);
	if(index == UNUSED)
		return LS_NO_SUCH_SCHEDULED_EVENT;
	scheduledEvents[index].id = UNUSED;
	return LS_OK;
}

LS_ErrorCodes LightScheduler_Randomize(int id, Day day, int minuteOfDay)
{
	int index = findScheduledEvent(id, day, minuteOfDay);
	if(index == UNUSED)
		return LS_NO_SUCH_SCHEDULED_EVENT;
	scheduledEvents[index].randomize = RANDOM_ON;
	scheduledEvents[index].randomMinutes = RandomMinute_Get(randomMinute);
	return LS_OK;
}

static BOOL isEventDueNow(Time time, ScheduledLightEvent* event)
{
	Day scheduledDay = event->day;
	int todaysMinute = event->minuteOfDay + event->randomMinutes;

	if(!Time_MatchesMinuteOfDay(time, todaysMinute))
		return FALSE;
	if(!Time_MatchesDayOfWeek(time, scheduledDay))
		return FALSE;
	return TRUE;
}

static void operateLight(ScheduledLightEvent* event)
{
	if (event->event == TURN_ON)
		LightController_On(event->id);
	else if (event->event == TURN_OFF)
		LightController_Off(event->id);
}
static void resetRandomize(ScheduledLightEvent* event)
{
	if (event->randomize == RANDOM_ON)
		event->randomMinutes = RandomMinute_Get(randomMinute);
	else
		event->randomMinutes = 0;
}
static void processEventDueNow(Time time, ScheduledLightEvent* event)
{
	if(!isInUse(event))
		return;
	if(isEventDueNow(time, event))
	{
		operateLight(event);
		resetRandomize(event);
	}
}
void LightScheduler_Wakeup(void)
{
	int i;
	TimeService_GetTime(time);

	for(i = 0; i < MAX_EVENTS; ++i)
	{
		processEventDueNow(time, &scheduledEvents[i]);
	}
}
