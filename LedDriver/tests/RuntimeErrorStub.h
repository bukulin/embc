#ifndef RuntimeErrorStub_h
#define RuntimeErrorStub_h 1

void RuntimeErrorStub_Reset(void);
const char* RuntimeErrorStub_GetLastError(void);
int RuntimeErrorStub_GetLastParameter(void);

#endif
