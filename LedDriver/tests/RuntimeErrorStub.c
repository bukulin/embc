#include "RuntimeErrorStub.h"
#include "RuntimeError.h"

static const char* message = "No Error";
static int parameter = -1;
static const char* file = 0;
static int line = -1;

void RuntimeErrorStub_Reset(void)
{
	message = "No Error";
	parameter = -1;
}

const char* RuntimeErrorStub_GetLastError(void)
{
	return message;
}

int RuntimeErrorStub_GetLastParameter(void)
{
	return parameter;
}

void RuntimeError(const char* aMessage, int aParameter,
		  const char* aFile, int aLine)
{
	message = aMessage;
	parameter = aParameter;
	file = aFile;
	line = aLine;
}
