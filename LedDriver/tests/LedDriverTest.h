#ifndef LedDriverTest_h
#define LedDriverTest_h 1

#include <CppUTest/TestHarness.h>

extern "C" {
#include "RuntimeErrorStub.h"
}

extern "C" {
#include "LedDriver.h"
}

TEST_GROUP(LedDriver)
{
	uint16_t virtualLeds;

	TEST_SETUP()
	{
		LedDriver_Create(&virtualLeds, FALSE);
	}

	TEST_TEARDOWN()
	{
	}
};

TEST(LedDriver, LedsOffAfterCreate)
{
	uint16_t virtualLeds = 0xffff;
	LedDriver_Create(&virtualLeds, FALSE);
	CHECK_EQUAL(0, virtualLeds);
}

TEST(LedDriver, TurnOnOneLed)
{
	LedDriver_TurnOn(1);
	CHECK_EQUAL(1, virtualLeds);
}

TEST(LedDriver, TurnOffAnyLed)
{
	LedDriver_TurnAllOn();
	LedDriver_TurnOff(8);
	CHECK_EQUAL(0xff7f, virtualLeds);
}

TEST(LedDriver, TurnOffOneLed)
{
	LedDriver_TurnOn(1);
	LedDriver_TurnOff(1);
	CHECK_EQUAL(0, virtualLeds);
}

TEST(LedDriver, TurnOnMultipleLeds)
{
	LedDriver_TurnOn(9);
	LedDriver_TurnOn(8);
	CHECK_EQUAL(0x180, virtualLeds);
}

TEST(LedDriver, TurnOffMultipleLeds)
{
	LedDriver_TurnAllOn();
	LedDriver_TurnOff(8);
	LedDriver_TurnOff(1);
	CHECK_EQUAL((~0x81)&0xffff, virtualLeds)
}

TEST(LedDriver, AllOn)
{
	LedDriver_TurnAllOn();
	CHECK_EQUAL(0xffff, virtualLeds);
}

TEST(LedDriver, AllOff)
{
	LedDriver_TurnAllOn();
	LedDriver_TurnAllOff();
	CHECK_EQUAL(0, virtualLeds);
}

TEST(LedDriver, LedMemoryIsNotReadable)
{
	virtualLeds = 0xffff;
	LedDriver_TurnOn(8);
	CHECK_EQUAL(0x80, virtualLeds);
}

TEST(LedDriver, UpperAndLowerBounds)
{
	LedDriver_TurnOn(1);
	LedDriver_TurnOn(16);
	CHECK_EQUAL(0x8001, virtualLeds);
}

TEST(LedDriver, OutOfBoundTurnOnDoesNotHarm)
{
	LedDriver_TurnOn(-1);
	LedDriver_TurnOn(0);
	LedDriver_TurnOn(17);
	LedDriver_TurnOn(3141);
	CHECK_EQUAL(0, virtualLeds);
}

TEST(LedDriver, OutOfBoundTurnOffDoesNotHarm)
{
	LedDriver_TurnAllOn();

	LedDriver_TurnOff(-1);
	LedDriver_TurnOff(0);
	LedDriver_TurnOff(17);
	LedDriver_TurnOff(3141);

	CHECK_EQUAL(0xffff, virtualLeds);
}

IGNORE_TEST(LedDriver, OutOfBoundsTurnOnProducesRuntimeErrors)
{
	/* TODO what should happen? */
}

IGNORE_TEST(LedDriver, OutOfBoundsTurnOffProducesRuntimeErrors)
{
	/* TODO what should happen? */
}

TEST(LedDriver, IsOn)
{
	CHECK_FALSE(LedDriver_IsOn(11));
	LedDriver_TurnOn(11);
	CHECK_TRUE(LedDriver_IsOn(11));
}

TEST(LedDriver, OutOfBoundsLedsAreAlwaysOff)
{
	CHECK_FALSE(LedDriver_IsOn(-1));
	CHECK_FALSE(LedDriver_IsOn(3125));
	CHECK_TRUE(LedDriver_IsOff(0));
	CHECK_TRUE(LedDriver_IsOff(3000));
}

TEST(LedDriver, IsOff)
{
	CHECK_TRUE(LedDriver_IsOff(3));
	LedDriver_TurnOn(3);
	CHECK_FALSE(LedDriver_IsOff(3));
}


TEST_GROUP(InvertedLedDriver)
{
	uint16_t virtualLeds;

	TEST_SETUP()
	{
		LedDriver_Create(&virtualLeds, TRUE);
	}
	TEST_TEARDOWN()
	{
	}

	uint16_t invert(uint16_t value)
	{
		return (~value)&0xffff;
	}
};
TEST(InvertedLedDriver, InvertedOnIsOff)
{
	LedDriver_TurnOn(1);
	CHECK_EQUAL(invert(1), virtualLeds);
}
TEST(InvertedLedDriver, InvertedOffIsOn)
{
	LedDriver_TurnAllOn();
	LedDriver_TurnOff(1);
	CHECK_EQUAL(invert(0xfffe), virtualLeds);
}


#endif
