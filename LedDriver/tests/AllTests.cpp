#include <CppUTest/CommandLineTestRunner.h>

#include "LedDriverTest.h"

int main(int argc, char* argv[])
{
	return CommandLineTestRunner::RunAllTests(argc, argv);
}
