#ifndef BOOL_h
#define BOOL_h 1

typedef int BOOL;
enum {FALSE = 0, TRUE = 1};

#endif // BOOL_h
