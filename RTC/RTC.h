#ifndef RTC_h
#define RTC_h 1

#include <stdint.h>

uint32_t MicroTime_Get(void);

#endif
