#include "LightDriver.h"
#include "LightDriverPrivate.h"

#include "BOOL.h"

#include <stdlib.h>

static BOOL isValid(LightDriver self)
{
	return self && self->vtable;
}

void LightDriver_Destroy(LightDriver self)
{
	if(isValid(self) && self->vtable->Destroy)
		self->vtable->Destroy(self);
}

void LightDriver_TurnOn(LightDriver self)
{
	if(isValid(self) && self->vtable->TurnOn)
		self->vtable->TurnOn(self);
}
void LightDriver_TurnOff(LightDriver self)
{
	if(isValid(self) && self->vtable->TurnOff)
		self->vtable->TurnOff(self);
}

void LightDriver_SetBrightness(LightDriver self, int level)
{
	if(isValid(self) && self->vtable->Brightness)
		self->vtable->Brightness(self, level);
}
