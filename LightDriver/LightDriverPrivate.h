#ifndef LightDriverPrivate_h
#define LightDriverPrivate_h 1

#include "LightDriver.h"

typedef struct LightDriverInterfaceStruct* LightDriverInterface;

typedef struct LightDriverStruct
{
	LightDriverInterface vtable;
	const char* type;
	int id;
} LightDriverStruct;


typedef struct LightDriverInterfaceStruct
{
	void (*TurnOn)(LightDriver);
	void (*TurnOff)(LightDriver);
	void (*Destroy)(LightDriver);
	void (*Brightness)(LightDriver, int);
} LightDriverInterfaceStruct;

#endif
