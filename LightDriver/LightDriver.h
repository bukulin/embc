#ifndef LightDriver_h
#define LightDriver_h 1

typedef struct LightDriverStruct* LightDriver;

void LightDriver_Destroy(LightDriver);
void LightDriver_TurnOn(LightDriver);
void LightDriver_TurnOff(LightDriver);
void LightDriver_SetBrightness(LightDriver, int level);

#endif
