#include "X10LightDriver.h"
#include "LightDriverPrivate.h"

#include <stdlib.h>

#define MAX_X10_MESSAGE_LENGTH 16

typedef struct X10LightDriverStruct* X10LightDriver;
typedef struct X10LightDriverStruct
{
	LightDriverStruct base;
	X10_HouseCode house;
	int unit;
	char message[MAX_X10_MESSAGE_LENGTH];
} X10LightDriverStruct;


static void formatTurnOnMessage(X10LightDriver self)
{
	return;
}
static void formatTurnOffMessage(X10LightDriver self)
{
	return;
}
static void sendMessage(X10LightDriver self)
{
	return;
}

static void turnOn(LightDriver base)
{
	X10LightDriver self = (X10LightDriver)base;
	formatTurnOnMessage(self);
	sendMessage(self);
}
static void turnOff(LightDriver base)
{
	X10LightDriver self = (X10LightDriver)base;
	formatTurnOffMessage(self);
	sendMessage(self);
}
static void destroy(LightDriver base)
{
	X10LightDriver self = (X10LightDriver)base;
	free(self);
}

static LightDriverInterfaceStruct interface =
{
	.TurnOn = turnOn,
	.TurnOff = turnOff,
	.Destroy = destroy,
	.Brightness = NULL
};

LightDriver X10LightDriver_Create(int id, X10_HouseCode code, int unit)
{
	X10LightDriver self = calloc(1, sizeof(X10LightDriverStruct));
	self->base.vtable = &interface;
	self->base.type = "X10";
	self->base.id = id;
	self->house = code;
	self->unit = unit;
	return (LightDriver)self;
}
