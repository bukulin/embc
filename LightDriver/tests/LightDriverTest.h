#ifndef LightDriverTest_h
#define LightDriverTest_h 1

#include <CppUTest/TestHarness.h>

extern "C"
{
#include "LightDriver.h"
#include "LightDriverPrivate.h"
}

#define NONSENSE_POINTER (LightDriver)~0
static LightDriver savedDriver = NONSENSE_POINTER;

static void shouldNotBeCalled(LightDriver self)
{
	savedDriver = self;
}

static void shouldNotBeCalledWithInt(LightDriver self, int level)
{
	savedDriver = self;
}

static LightDriverInterfaceStruct interface =
{
	shouldNotBeCalled,
	shouldNotBeCalled,
	shouldNotBeCalled,
	shouldNotBeCalledWithInt
};

static LightDriverStruct testDriver;

TEST_GROUP(LightDriver)
{
	TEST_SETUP()
	{
		resetTestDriver();
	}

	TEST_TEARDOWN()
	{
	}

	void resetTestDriver()
	{
		testDriver.vtable = &interface;
		testDriver.type = "TestLightDriver";
		testDriver.id = 13;
	}
};

TEST(LightDriver, NullDriverDoesNotCrash)
{
	LightDriver_TurnOn(NULL);
	LightDriver_TurnOff(NULL);
	LightDriver_SetBrightness(NULL, 10);
	LightDriver_Destroy(NULL);
	POINTERS_EQUAL(NONSENSE_POINTER, savedDriver);
}

TEST(LightDriver, NullInterfaceDoesNotCrash)
{
	testDriver.vtable = NULL;
	LightDriver_TurnOn(&testDriver);
	LightDriver_TurnOff(&testDriver);
	LightDriver_Destroy(&testDriver);
	LightDriver_SetBrightness(&testDriver, 10);
	POINTERS_EQUAL(NONSENSE_POINTER, savedDriver);
}

TEST(LightDriver, NullBrightnessFunctionPointerDoesNotCrash)
{
	testDriver.vtable->Brightness = NULL;
	LightDriver_SetBrightness(&testDriver, 10);
	POINTERS_EQUAL(NONSENSE_POINTER, savedDriver);
}

#endif
