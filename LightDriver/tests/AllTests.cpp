#include <CppUTest/CommandLineTestRunner.h>

#include "LightDriverTest.h"

int main(int argc, char* argv[])
{
	return CommandLineTestRunner::RunAllTests(argc, argv);
}
