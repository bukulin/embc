find_package(PkgConfig)
pkg_check_modules(CPPUTEST REQUIRED cpputest)

include_directories(
  ${LightScheduler_SOURCE_DIR}/LightDriver/
  SYSTEM ${CPPUTEST_INCLUDE_DIRS})
link_directories(
  ${LightScheduler_BINARY_DIR}/LightDriver/
  ${CPPUTEST_LIBRARY_DIRS})

add_executable(LightDriverTest
  AllTests.cpp)

target_link_libraries(LightDriverTest
  LightDriver
  ${CPPUTEST_LIBRARIES})
