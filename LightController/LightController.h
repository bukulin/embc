#ifndef LightController_h
#define LightController_h 1

#include "LightDriver.h"
#include "BOOL.h"

#define NUMBER_OF_LIGHTS 32

void LightController_Create();
void LightController_Destroy();

BOOL LightController_Add(int id, LightDriver);

void LightController_On(int id);
void LightController_Off(int id);

#endif
