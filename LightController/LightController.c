#include "LightController.h"

#include "LightDriver.h"

#include <stdlib.h>
#include <string.h>

#define MAX_LIGHTS 32

static LightDriver lightDrivers[MAX_LIGHTS] = {NULL};

void LightController_Create(void)
{
	memset(lightDrivers, 0, sizeof(lightDrivers));
}

void LightController_Destroy(void)
{
	int i;
	for(i = 0; i < MAX_LIGHTS; ++i)
	{
		LightDriver_Destroy(lightDrivers[i]);
		lightDrivers[i] = NULL;
	}
}

BOOL LightController_Add(int id, LightDriver base)
{
	LightDriver_Destroy(lightDrivers[id]);
	lightDrivers[id] = base;
}

void LightController_On(int id)
{
	LightDriver_TurnOn(lightDrivers[id]);
}

void LightController_Off(int id)
{
	LightDriver_TurnOff(lightDrivers[id]);
}
