#ifndef LightControllerTest_h
#define LightControllerTest_h 1

#include <CppUTest/TestHarness.h>

extern "C"
{
#include "LightController.h"
#include "LightDriverSpy.h"
#include "CountingLightDriver.h"
}

TEST_GROUP(LightController)
{
	LightDriver spy;

	TEST_SETUP()
	{
		LightController_Create();
		LightDriverSpy_AddSpiesToController();
		LightDriverSpy_Reset();
	}

	TEST_TEARDOWN()
	{
		LightController_Destroy();
	}
};

TEST(LightController, TurnOn)
{
	LightController_On(7);
	LONGS_EQUAL(LIGHT_ON, LightDriverSpy_GetState(7));
}

TEST(LightController, TurnOff)
{
	LightController_Off(4);
	LONGS_EQUAL(LIGHT_OFF, LightDriverSpy_GetState(4));
}

TEST(LightController, AddingDriverDestroysPrevious)
{
	LightDriver spy = LightDriverSpy_Create(1);
	LightController_Add(1, spy);
	LightController_Destroy();
}

TEST(LightController, TurnOnDifferentDriverTypes)
{
	LightDriver otherDriver = CountingLightDriver_Create(5);
	LightController_Add(5, otherDriver);
	LightController_On(7);
	LightController_On(5);
	LightController_Off(5);
	LONGS_EQUAL(LIGHT_ON, LightDriverSpy_GetState(7));
	LONGS_EQUAL(2, CountingLightDriver_GetCallCount(otherDriver));
}

#endif
