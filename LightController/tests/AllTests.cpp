#include <CppUTest/CommandLineTestRunner.h>

#include "LightControllerTest.h"
#include "LightDriverSpyTest.h"

int main(int argc, char* argv[])
{
	return CommandLineTestRunner::RunAllTests(argc, argv);
}
