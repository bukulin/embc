#ifndef LightDriverSpy_h
#define LightDriverSpy_h 1

#include "LightDriver.h"

enum
{
	LIGHT_ID_UNKNOWN = -1,
	LIGHT_STATE_UNKNOWN = -1,
	LIGHT_OFF = 0,
	LIGHT_ON = 1
};

LightDriver LightDriverSpy_Create(int id);

void LightDriverSpy_Reset(void);
int LightDriverSpy_GetState(int id);
int LightDriverSpy_GetLastId(int id);
int LightDriverSpy_GetLastState(int id);
void LightDriverSpy_AddSpiesToController(void);

#endif
