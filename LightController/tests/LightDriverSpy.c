#include "LightDriverSpy.h"
#include "LightDriverPrivate.h"

#include <stdlib.h>

typedef struct LightDriverSpyStruct* LightDriverSpy;
typedef struct LightDriverSpyStruct
{
	LightDriverStruct base;
} LightDriverSpyStruct;

#define MAX_LIGHTS 32

int states[MAX_LIGHTS];
int lastId;
int lastState;

static void destroy(LightDriver base)
{
	LightDriverSpy self = (LightDriverSpy)base;
	if(self == NULL)
		return;
	free(self);
}

static void turnOn(LightDriver base)
{
	LightDriverSpy self = (LightDriverSpy)base;
	states[self->base.id] = LIGHT_ON;
	lastId = self->base.id;
	lastState = LIGHT_ON;
}
static void turnOff(LightDriver base)
{
	LightDriverSpy self = (LightDriverSpy)base;
	states[self->base.id] = LIGHT_OFF;
	lastId = self->base.id;
	lastState = LIGHT_OFF;
}

static LightDriverInterfaceStruct interface =
{
	.TurnOn = turnOn,
	.TurnOff = turnOff,
	.Destroy = destroy
};

LightDriver LightDriverSpy_Create(int id)
{
	LightDriverSpy self = calloc(1, sizeof(LightDriverSpyStruct));
	self->base.vtable = &interface;
	self->base.type = "Spy";
	self->base.id = id;
	return (LightDriver)self;
}

void LightDriverSpy_Reset(void)
{
	int i;

	for(i = 0; i < MAX_LIGHTS; ++i)
		states[i] = LIGHT_STATE_UNKNOWN;
	lastId = LIGHT_ID_UNKNOWN;
	lastState = LIGHT_STATE_UNKNOWN;
}

int LightDriverSpy_GetState(int id)
{
	return states[id];
}

void LightDriverSpy_AddSpiesToController(void)
{
	int i;
	LightDriver driver;

	for(i = 0; i < MAX_LIGHTS; ++i)
	{
		driver = LightDriverSpy_Create(i);
		LightController_Add(i, driver);
	}
}
