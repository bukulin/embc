#ifndef LightDriverCountingSpy_h
#define LightDriverCountingSpy_h 1

#include "LightDriver.h"

LightDriver CountingLightDriver_Create(int id);
int CountingLightDriver_GetCallCount(LightDriver);

#endif
