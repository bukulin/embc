#ifndef LightDriverSpyTest_h
#define LightDriverSpyTest_h 1

#include <CppUTest/TestHarness.h>

extern "C"
{
#include "LightDriverSpy.h"
}

TEST_GROUP(LightDriverSpy)
{
	LightDriver lightDriverSpy;

	TEST_SETUP()
	{
		lightDriverSpy = LightDriverSpy_Create(1);
		LightDriverSpy_Reset();
	}

	TEST_TEARDOWN()
	{
		LightDriver_Destroy(lightDriverSpy);
	}

};

TEST(LightDriverSpy, On)
{
	LightDriver_TurnOn(lightDriverSpy);
	LONGS_EQUAL(LIGHT_ON, LightDriverSpy_GetState(1));
}

TEST(LightDriverSpy, Off)
{
	LightDriver_TurnOff(lightDriverSpy);
	LONGS_EQUAL(LIGHT_OFF, LightDriverSpy_GetState(1));
}


#endif
