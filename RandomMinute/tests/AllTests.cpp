#include <CppUTest/CommandLineTestRunner.h>

#include "RandomMinuteTest.h"
#include "ProductionRandomMinuteTest.h"

int main(int argc, char* argv[])
{
	return CommandLineTestRunner::RunAllTests(argc, argv);
}
