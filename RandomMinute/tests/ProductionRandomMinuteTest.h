#ifndef ProductionRandomMinuteTest_h
#define ProductionRandomMinuteTest_h 1

#include <stdio.h>
#include <string.h>
#include <CppUTest/TestHarness.h>

extern "C"
{
#include "RandomMinute.h"
#include "ProductionRandomMinute.h"
}

enum {BOUND = 30};

TEST_GROUP(ProductionRandomMinute)
{
	int minute;
	RandomMinute randomMinute;

	TEST_SETUP()
	{
		randomMinute = ProductionRandomMinute_Create(BOUND);
	}

	TEST_TEARDOWN()
	{
		RandomMinute_Destroy(randomMinute);
	}

	void AssertMinuteIsInRange()
	{
		if( minute < -BOUND || minute > BOUND )
		{
			printf("bad minute value: %d\n", minute);
			FAIL("Minute out of range");
		}
	}
};

TEST(ProductionRandomMinute, GetIsInRange)
{
	for(int i = 0; i < 100; ++i)
	{
		minute = RandomMinute_Get(randomMinute);
		AssertMinuteIsInRange();
	}
}

TEST(ProductionRandomMinute, AllValuesPossible)
{
	int hit[2*BOUND + 1];
	memset(hit, 0, sizeof(hit));

	int i;
	for(i = 0; i < 1023; ++i)
	{
		minute = RandomMinute_Get(randomMinute);
		AssertMinuteIsInRange();
		hit[minute + BOUND]++;
	}

	for(i = 0; i < 2*BOUND; ++i)
	{
		CHECK(hit[i] > 0);
	}
}

TEST(ProductionRandomMinute, FirstGetOfTwoInstancesMustDiffer)
{
	minute = RandomMinute_Get(randomMinute);
	RandomMinute other = ProductionRandomMinute_Create(BOUND);
	int otherMinute = RandomMinute_Get(other);
	RandomMinute_Destroy(other);
	CHECK(minute != otherMinute);
}


#endif
