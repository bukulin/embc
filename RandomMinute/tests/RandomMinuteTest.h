// -*- mode: c++ -*-
#ifndef RandomMinuteTest_h
#define RandomMinuteTest_h 1

#include <CppUTest/TestHarness.h>

extern "C"
{
#include "RandomMinute.h"
#include "RandomMinutePrivate.h"
}

#define NONSENSE_POINTER (RandomMinute)~0
static RandomMinute savedInstancePointer = NONSENSE_POINTER;

static void shouldNotBeCalledDestructor(RandomMinute self)
{
	savedInstancePointer = self;
}
static int shouldNotBeCalledGet(RandomMinute self)
{
	savedInstancePointer = self;
	return -3;
}
static RandomMinuteInterfaceStruct interface =
{
	shouldNotBeCalledDestructor,
	shouldNotBeCalledGet
};


TEST_GROUP(RandomMinute)
{
	RandomMinuteStruct randomMinute;

	TEST_SETUP()
	{
		resetRandomMinute();
	}

	TEST_TEARDOWN()
	{
	}

	void resetRandomMinute()
	{
		randomMinute.vtable = &interface;
	}
};

TEST(RandomMinute, NullInstancePointerDoesNotCrash)
{
	RandomMinute_Get(NULL);
	RandomMinute_Destroy(NULL);
	POINTERS_EQUAL(NONSENSE_POINTER, savedInstancePointer);
}

TEST(RandomMinute, NullVtablePointerDoesNotCrash)
{
	randomMinute.vtable = NULL;
	RandomMinute_Get(&randomMinute);
	RandomMinute_Destroy(&randomMinute);
	POINTERS_EQUAL(NONSENSE_POINTER, savedInstancePointer);
}

#endif
