#include "ProductionRandomMinute.h"
#include "RandomMinutePrivate.h"

#include <stdlib.h>

static int seedCounter = 0;

typedef struct ProductionRandomMinuteStruct* ProductionRandomMinute;
typedef struct ProductionRandomMinuteStruct
{
	RandomMinuteStruct base;
	int bound;
	int seed;
} ProductionRandomMinuteStruct;

static void destroy(RandomMinute base)
{
	free(base);
}
static int get(RandomMinute base)
{
	ProductionRandomMinute self = (ProductionRandomMinute)base;
	return (rand_r(&self->seed) % (2 * self->bound)) - self->bound;
}

static RandomMinuteInterfaceStruct interface =
{
	.Destroy = destroy,
	.Get = get
};

RandomMinute ProductionRandomMinute_Create(int aBound)
{
	ProductionRandomMinute self = calloc(1, sizeof(ProductionRandomMinuteStruct));
	self->base.vtable = &interface;
	self->bound = aBound;
	self->seed = seedCounter++;
	srand(self->seed);
	return (RandomMinute)self;
}
