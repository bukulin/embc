#ifndef RandomMinute_h
#define RandomMinute_h 1

typedef struct RandomMinuteStruct* RandomMinute;

void RandomMinute_Destroy(RandomMinute);
int RandomMinute_Get(RandomMinute);

#endif
