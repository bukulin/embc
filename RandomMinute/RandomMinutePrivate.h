#ifndef RandomMinutePrivate_h
#define RandomMinutePrivate_h 1

typedef struct RandomMinuteInterfaceStruct* RandomMinuteInterface;

typedef struct RandomMinuteStruct
{
	RandomMinuteInterface vtable;
} RandomMinuteStruct;

typedef struct RandomMinuteInterfaceStruct
{
	void (*Destroy)(RandomMinute);
	int (*Get)(RandomMinute);
} RandomMinuteInterfaceStruct;

#endif
