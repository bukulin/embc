#include "RandomMinute.h"
#include "RandomMinutePrivate.h"

#include "BOOL.h"

#include <stdlib.h>

static BOOL isValid(RandomMinute self)
{
	return self && self->vtable;
}
void RandomMinute_Destroy(RandomMinute self)
{
	if(isValid(self))
		self->vtable->Destroy(self);
}
int RandomMinute_Get(RandomMinute self)
{
	if(isValid(self))
		self->vtable->Get(self);
}
